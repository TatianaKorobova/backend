# Installation

To install the project, run:

```bash
npm install
```

To load initial data into the database, run:

```bash
npm run bootstrap
```

# Usage
Update .env if necessary

To start server, run:
```bash
npm start
```