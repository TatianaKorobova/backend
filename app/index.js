const express    = require('express');
const path       = require('path');
const bodyParser = require('body-parser');
const cors       = require('cors');

const countriesRouter = require('./routers/countriesRouter');
const sequelize       = require('./db-access/sequelize');

const { HOST, PORT } = process.env;

async function init() {
  try {
    await sequelize.sync();

    const app = express();

    app.use(cors());
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: true}));

    app.use('/assets', express.static(path.join(__dirname, 'assets')));
    app.use('/countries', countriesRouter);

    app.listen(PORT, HOST, () => {
      console.log(`Server has started on http://${HOST}:${PORT}`);
    });

  } catch (error) {
    await sequelize.close();
    console.log(error);
  }
}

init();