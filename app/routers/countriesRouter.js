const express = require('express');

const {
  getCountries,
  updateCountry,
  deleteCountry
} = require('../controllers/countriesController');

const router = express.Router();

router.get("/", getCountries);
router.put("/", updateCountry);
router.delete("/:code", deleteCountry);

module.exports = router;