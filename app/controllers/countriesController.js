const countriesService = require('../db-access/db-services/countriesService');

const getCountries = async (req, res) => {
  try {
    const {page, rows, searchValue, searchProp} = req.query;
    const countries                             = await countriesService.getCountries(rows, page, searchValue, searchProp);
    res.status(200).json(countries);
  } catch (error) {
    return res.status(400).json({
      error:   'something_went_wrong',
      message: error.message
    });
  }
}

const updateCountry = async (req, res) => {
  try {
    const {country} = req.body;
    await countriesService.updateCountry(country, country.code);
    res.status(200).end();
  } catch (error) {
    return res.status(400).json({
      error:   'something_went_wrong',
      message: error.message
    });
  }
}

const deleteCountry = async (req, res) => {
  try {
    const {code}                                = req.params;
    const {page, rows, searchValue, searchProp} = req.query;

    const deletedCountry = await countriesService.getCountryByProps({code});
    await countriesService.deleteCountry(deletedCountry);
    const countries = await countriesService.getCountries(rows, page, searchValue, searchProp);
    res.status(200).json(countries);
  } catch (error) {
    return res.status(400).json({
      error:   'something_went_wrong',
      message: error.message
    });
  }
}

module.exports = {
  getCountries,
  updateCountry,
  deleteCountry
}