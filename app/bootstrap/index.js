const mysql = require('mysql2/promise');
const dotenv = require('dotenv');
dotenv.config();

const countries = require('./data/countries.js');
const { Country, CountrySchema } = require('../db-access/models/Country');
const sequelize = require('../db-access/sequelize');

const {
  DB_HOST,
  DB_PORT,
  DB_NAME,
  DB_USERNAME,
  DB_PASSWORD,
  HOST,
  PORT
} = process.env;

const init = async () => {
  try {
    const queryInterface = sequelize.getQueryInterface();

    const connection = await mysql.createConnection({
      host:     DB_HOST,
      port:     DB_PORT,
      user:     DB_USERNAME,
      password: DB_PASSWORD,
    });

    await connection.query(`CREATE DATABASE IF NOT EXISTS ${DB_NAME}`);
    await queryInterface.createTable('countries', CountrySchema);

    for ({ name, country_code, latlng} of countries) {
      await Country.create({ name, code: country_code, latlng })
    }
  } catch (error) {
    console.log(error)
  } finally {
    process.exit(0);
  }

}

init();