const { DataTypes } = require('sequelize');
const sequelize = require('../sequelize');

const CountrySchema = {
  code:        {
    primaryKey: true,
    allowNull:  false,
    type:       DataTypes.STRING
  },
  name:        DataTypes.STRING,
  latlng:      {
    type: DataTypes.STRING,
    get() {
      return JSON.parse(this.getDataValue('latlng'));
    },
    set(coords) {
      this.setDataValue('latlng', JSON.stringify(coords))
    }
  },
  description: DataTypes.STRING
}

const Country = sequelize.define(
  'Country',
  CountrySchema,
  { timestamps: false }
);

module.exports = { Country, CountrySchema };
