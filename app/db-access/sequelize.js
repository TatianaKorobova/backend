const { Sequelize } = require('sequelize');
const dotenv = require('dotenv');
dotenv.config();

const { DB_NAME, DB_USERNAME, DB_PASSWORD, DB_HOST } = process.env;

const sequelizeConfig = {
  host:    DB_HOST,
  dialect: 'mysql'
};

const sequelize = new Sequelize(DB_NAME, DB_USERNAME, DB_PASSWORD, sequelizeConfig);

module.exports = sequelize;