const {Country} = require('../models/Country');
const {Op}      = require("sequelize");

const getCountries = (rows, page, searchValue, searchProp) => {
  let params = rows && page ? {limit: +rows, offset: +rows * +page} : {};
  if (searchValue && searchValue !== '' && searchProp) {
    params = {
      ...params,
      where: {[searchProp]: {[Op.like]: `%${searchValue}%`}}
    }
  }
  return Country.findAndCountAll(params);
}

const getCountryByProps = (props) => {
  return Country.findOne({where: props});
}

const updateCountry = (country, code) => {
  return Country.update(country, {where: {code}})
}

const deleteCountry = (country) => {
  return country.destroy();
}

module.exports = {
  getCountries,
  getCountryByProps,
  updateCountry,
  deleteCountry
}